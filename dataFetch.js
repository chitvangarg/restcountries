const globalApi = `https://restcountries.com/v3.1/all`

export const completeDataFetch = async function () {
    try {
        const countries = await fetch(globalApi)

        if (countries) {
            return countries.json()
        } else {
            throw new Error("Unable to fetch data")
        }
    } catch (err) {
        throw new Error(err)
    }
}

export const paramsDataFetch = async function (id) {
    try {
        if(id !== undefined){
            const country = await fetch(
                `https://restcountries.com/v3.1/alpha/${id}`
            )

            if (country) {
                return country.json()
            } else {
                throw new Error("Unable to fetch data")
            }
        }else{
            throw new Error("Unable to fetch data")
        }
    } catch (err) {
        throw new Error(err)
    }
}
