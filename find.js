export const bySearch = (searchedCountry, country)  => {
    return (
        searchedCountry === "" ||
        country.name["common"]
            .toLowerCase()
            .includes(searchedCountry.toLowerCase())
    )
}

export const byRegion = (region, country) => {
    return region === "" || country["region"] === region
}

export const bySubRegion = (subRegion, country) => {
    return subRegion === "" || country["subregion"] === subRegion
}

