function sortCountries(way, property, countries) {
    countries.sort((country1, country2) =>
        way === "Ascending"
            ? country1[property] - country2[property]
            : country2[property] - country1[property]
    )
}

export default sortCountries