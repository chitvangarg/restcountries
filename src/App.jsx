import { useContext } from "react"
import Home from "./components/Home"
import { Routes, Route } from "react-router-dom"
import Details from "./components/Details"
import  {ThemeProvider, themeContext }  from "./ThemeProvider"


function App() {

    return (
        <ThemeProvider >
                <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path="/country/:id" element={<Details />} />
                </Routes>
        </ThemeProvider>
    )
}

export default App
