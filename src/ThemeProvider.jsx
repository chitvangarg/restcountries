import { useState, createContext } from "react"
import Header from "./components/Header"

export const themeContext = createContext()

export const ThemeProvider = ({ children }) => {
    const [darkTheme, setDarkTheme] = useState("light")

    const handleThemeChange = () => {
        setDarkTheme((theme) => (theme === "dark" ? "light" : "dark"))
    }

    const themeStyling = {
        dark: {
            background: "hsl(207, 26%, 17%)",
            elements: "hsl(209, 23%, 22%)",
            text: "hsl(0, 0%, 100%)",
        },
        light: {
            background: "hsl(0, 0%, 98%)",
            elements: "hsl(0, 0%, 100%)",
            text: "hsl(200, 15%, 8%)",
        },
    }

    return (
        <themeContext.Provider value={{ darkTheme, themeStyling }}>
            <div style={{backgroundColor: `${themeStyling[darkTheme]["background"]}`, minHeight: `100vh`}}>
                <Header changeTheme={handleThemeChange} />
                {children}
            </div>
        </themeContext.Provider>
    )
}
