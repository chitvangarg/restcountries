import CountryCard from "./CountryCard"

function Countries({countries}) {

    return (
        <div className="country_container">
            {countries.map((country, index) => {
                return <CountryCard {...country} key={index} />
            })}
        </div>
    )
}

export default Countries
