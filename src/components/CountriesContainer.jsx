import Countries from "./Countries"

const CountriesContainer = ({ countries }) => {
    return (
        <div className="container">
            <Countries countries={countries}  />
        </div>
    )
}

export default CountriesContainer
