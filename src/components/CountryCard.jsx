import React from "react"
import { useContext } from "react"
import { useNavigate } from "react-router-dom"
import { themeContext } from "../ThemeProvider"

const CountryCard = (props) => {
    const { name, population, region, capital, flags, subregion, area, cca3 } =
        props

    const theme = useContext(themeContext)

    const navigate = useNavigate()

    const background = theme["themeStyling"][theme["darkTheme"]]["elements"]
    const textColor = theme["themeStyling"][theme["darkTheme"]]["text"]

    return (
        <div
            className="card"
            style={{ backgroundColor: `${background}`, color: `${textColor}` }}
            onClick={() => {
                navigate(`/country/${cca3}`)
            }}
        >
            <img
                src={flags["png"]}
                alt={flags["alt"]}
                className="country_flag"
            />
            <div className="info">
                {name.common && <h3 className="title">{name.common}</h3>}
                {
                    <h4 className="infos">
                        population:{" "}
                        <span style={{ fontWeight: 300 }}>{population}</span>
                    </h4>
                }
                {region && (
                    <h4 className="infos">
                        region:{" "}
                        <span style={{ fontWeight: 300 }}>{region}</span>
                    </h4>
                )}
                {capital && (
                    <h4 className="infos">
                        capital:{" "}
                        <span style={{ fontWeight: 300 }}>{capital}</span>
                    </h4>
                )}
                {subregion && (
                    <h4 className="infos">
                        SubRegion:{" "}
                        <span style={{ fontWeight: 300 }}>{subregion}</span>
                    </h4>
                )}
                {area && (
                    <h4 className="infos">
                        Area: <span style={{ fontWeight: 300 }}>{area}</span>
                    </h4>
                )}
            </div>
        </div>
    )
}

export default CountryCard
