import React, { useEffect } from "react"
import { useContext, useState } from "react"
import { Link, useNavigate, useParams } from "react-router-dom"
import { themeContext } from "../ThemeProvider"
import DarkBack from "../assets/left-arrow-dark.png"
import LightBack from "../assets/left-arrow-light.png"
import "../DetailPage.css"
import { paramsDataFetch } from "../../dataFetch"

const Details = () => {
    const [country, setCountryInfo] = useState([])
    const [isLoading, setLoading] = useState(true)
    const [isError, setError] = useState(false)

    const theme = useContext(themeContext)

    const navigate = useNavigate()

    const { id } = useParams()

    useEffect(() => {
        paramsDataFetch(id)
            .then((countries) => {
                setCountryInfo(countries)
                setLoading(false)
            })
            .catch((err) => {
                setError(true)
            })
    }, [id])

    const background = theme["themeStyling"][theme["darkTheme"]]["background"]
    const textColor = theme["themeStyling"][theme["darkTheme"]]["text"]
    const element = theme["themeStyling"][theme["darkTheme"]]["elements"]

    if (isLoading) {
        return <span className="loader"></span>
    } else if (isError) {
        return <Error error="No Data Found !" />
    } else {
        return (
            <>
                <div
                    className="detailsContainer"
                    style={{
                        backgroundColor: `${background}`,
                        color: `${textColor}`,
                    }}
                >
                    <div className="backButton">
                        <button
                            onClick={() => navigate("/")}
                            style={{
                                backgroundColor: `${element}`,
                                color: `${textColor}`,
                            }}
                        >
                            <img
                                src={
                                    theme["darkTheme"] === "light"
                                        ? DarkBack
                                        : LightBack
                                }
                            />
                            <span>Back</span>
                        </button>
                    </div>
                    <div className="countryDetails">
                        {country.map((elem) => (
                            <>
                                <div className="imageBox">
                                    <img
                                        src={elem["flags"]["svg"]}
                                        alt={elem["flags"]["alt"]}
                                    />
                                </div>
                                <div className="contentBox">
                                    {elem["name"].common && (
                                        <h2>{elem["name"].common}</h2>
                                    )}
                                    <div className="informations">
                                        <ul>
                                            <li>
                                                <span className="points">
                                                    Native name:{" "}
                                                </span>
                                                {elem.name["nativeName"] && (
                                                    <>
                                                        {Object.values(
                                                            elem.name[
                                                                "nativeName"
                                                            ]
                                                        )
                                                            .map(
                                                                (name) =>
                                                                    name.common
                                                            )
                                                            .join(",")}
                                                    </>
                                                )}
                                            </li>
                                            <li>
                                                {elem.population && (
                                                    <>
                                                        <span className="points">
                                                            Population:
                                                        </span>
                                                        (
                                                        <span>
                                                            {elem.population}
                                                        </span>
                                                        )
                                                    </>
                                                )}
                                            </li>
                                            <li>
                                                {elem.region && (
                                                    <>
                                                        <span className="points">
                                                            Region:{" "}
                                                        </span>
                                                        <span>
                                                            {" "}
                                                            {elem.region}
                                                        </span>
                                                    </>
                                                )}
                                            </li>
                                            <li>
                                                {elem.subregion && (
                                                    <>
                                                        <span className="points">
                                                            Sub Region:
                                                        </span>
                                                        <span>
                                                            {" "}
                                                            {elem.subregion}
                                                        </span>
                                                    </>
                                                )}
                                            </li>
                                            <li>
                                                {elem.capital && (
                                                    <>
                                                        <span className="points">
                                                            Capital:
                                                        </span>
                                                        <span>
                                                            {" "}
                                                            {elem.capital.join(
                                                                ","
                                                            )}
                                                        </span>
                                                    </>
                                                )}
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                {elem.tld && (
                                                    <>
                                                        <span className="points">
                                                            Top Level Domain:
                                                        </span>
                                                        <span>
                                                            {" "}
                                                            {elem.tld.join(",")}
                                                        </span>
                                                    </>
                                                )}
                                            </li>
                                            <li>
                                                {elem.currencies && (
                                                    <>
                                                        <span className="points">
                                                            Currencies:{" "}
                                                        </span>
                                                        <span>
                                                            {" "}
                                                            {Object.values(
                                                                elem.currencies
                                                            )
                                                                .map(
                                                                    (
                                                                        currency
                                                                    ) =>
                                                                        currency.name
                                                                )
                                                                .join(",")}
                                                        </span>
                                                    </>
                                                )}
                                            </li>
                                            <li>
                                                {elem.languages && (
                                                    <>
                                                        <span className="points">
                                                            Languages:
                                                        </span>
                                                        <span>
                                                            {" "}
                                                            {Object.values(
                                                                elem.languages
                                                            ).join(",")}
                                                        </span>
                                                    </>
                                                )}
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="borderCountries">
                                        {elem.borders && (
                                            <>
                                                <span>Borders: </span>
                                                <div className="borderCountry">
                                                    {elem["borders"].map(
                                                        (border) => {
                                                            return (
                                                                <Link to= {`/country/${border}`}
                                                                    className="border"
                                                                    style={{
                                                                        backgroundColor: `${element}`,
                                                                        color: `${textColor}`,
                                                                    }}
                                                                >
                                                                    {border}
                                                                </Link>
                                                            )
                                                        }
                                                    )}
                                                </div>
                                            </>
                                        )}
                                    </div>
                                </div>
                            </>
                        ))}
                    </div>
                </div>
            </>
        )
    }
}

export default Details
