import React from 'react'
import ErrorIcon from '../assets/error.png'
import { useContext } from 'react'
import { themeContext } from '../ThemeProvider'

const Error = ({error}) => {

  const theme = useContext(themeContext)

  const textColor = theme["themeStyling"][theme["darkTheme"]]["text"]

  return (
    <div className='ErrorContainer'>
        <img src={ErrorIcon} alt="Error" />
        <h2 style={{color: `${textColor}`}}>{error}</h2>
    </div>
  )
}

export default Error