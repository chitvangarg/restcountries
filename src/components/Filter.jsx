import React from "react"
import { useContext } from "react"
import { themeContext } from "../ThemeProvider"

const Filter = ({ options, selector, value, text }) => {
    const theme = useContext(themeContext)

    const background = theme["themeStyling"][theme["darkTheme"]]["elements"]
    const textColor = theme["themeStyling"][theme["darkTheme"]]["text"]

    return (
        <>
            {options && options.length !== 0 && (
                <div className="filter_input">
                    <select
                        value={value}
                        onChange={(e) => selector(e.target.value)}
                        style={{
                            backgroundColor: `${background}`,
                            color: `${textColor}`,
                        }}
                    >
                        <option value="" defaultValue hidden>
                            Filter by {text}
                        </option>
                        {[...new Set(options)].map((countryData, index) => {
                            return (
                                <option value={countryData} key={index}>
                                    {countryData}
                                </option>
                            )
                        })}
                    </select>
                </div>
            )}
        </>
    )
}

export default Filter
