import Dark from "../assets/night-mode.png"
import Light from "../assets/lightbulb.png"
import { useContext } from "react"
import { themeContext } from "../ThemeProvider"

const Header = ({ changeTheme }) => {
    const theme = useContext(themeContext)

    const background = theme["themeStyling"][theme["darkTheme"]]["elements"]
    const textColor = theme["themeStyling"][theme["darkTheme"]]["text"]

    return (
        <div
            className="header"
            style={{ backgroundColor: `${background}`, color: `${textColor}` }}
        >
            <div className="logo">
                <h2>Where in the World?</h2>
            </div>
            <div className="theme_mode" onClick={changeTheme}>
                <img
                    src={theme["darkTheme"] === "light" ? Dark : Light}
                    alt=""
                />
                <button
                    style={{
                        backgroundColor: `${background}`,
                        color: `${textColor}`,
                    }}
                >
                    {theme["darkTheme"] === "light"
                        ? "Dark mode"
                        : "Light mode"}
                </button>
            </div>
        </div>
    )
}

export default Header
