import CountriesContainer from "./CountriesContainer"
import "../Homepage_style.css"
import SearchBar from "./SearchBar"
import { useEffect, useState } from "react"
import Error from "./Error"
import { bySearch, byRegion, bySubRegion } from "../../find"
import sortCountries from "../../sort"
import { completeDataFetch } from "../../dataFetch"

const Home = () => {
    const [countryContent, setCountryContent] = useState([])
    const [searchedCountry, setSearchedCountry] = useState("")
    const [Region, setRegion] = useState("")
    const [SubRegion, setSubRegion] = useState("")
    const [isLoading, setLoading] = useState(true)
    const [sortBy, setSortby] = useState("Sort by")
    const [isError, setError] = useState(false)

    useEffect(() => {
        completeDataFetch().then((res) => {
            setCountryContent(res)
            setLoading(false)
        }).catch((err)=>{
            setLoading(false)
            setError(true) 
        })
    }, [])

    // unique regions
    let uniqueRegion = countryContent.reduce((regions, currCountry) => {
        if (
            currCountry.region !== undefined &&
            regions[currCountry.region] === undefined
        ) {
            const region = []

            if (currCountry.subregion !== undefined) {
                region.push(currCountry.subregion)
            }
            regions[currCountry.region] = region
        } else if (currCountry.subregion !== undefined) {
            regions[currCountry.region].push(currCountry.subregion)
        }

        return regions
    }, {})

    // State handling Callbacks
    const handleSearchCountry = (country) => {
        setSearchedCountry(country)
    }

    const handleRegion = (region) => {
        setRegion(region)
        setSubRegion("")
    }

    const handleSubregion = (subregion) => {
        setSubRegion(subregion)
    }

    const handleSortBy = (pattern) => {
        setSortby(pattern)
    }

    let filteredCountries = countryContent
        .filter((country) => bySearch(searchedCountry, country))
        .filter((country) => byRegion(Region, country))
        .filter((country) => bySubRegion(SubRegion, country))

    // Sorting of countries

    if (sortBy != "Sort by") {
        if (sortBy === "Area (low to high)") {
            sortCountries("Ascending", "area", countryContent)
        } else if (sortBy === "Population (low to high)") {
            sortCountries("Ascending", "population", countryContent)
        } else if (sortBy === "Area (high to low)") {
            sortCountries("Descending", "area", countryContent)
        } else if (sortBy === "Population (high to low)") {
            sortCountries("Descending", "population", countryContent)
        }
    }

    if (isLoading) {
        return <span className="loader"></span>
    } else if (isError) {
        return <Error error="No data fetched" />
    } else {
        return (
            <>
                <SearchBar
                    uniqueRegion={uniqueRegion}
                    setSearchedCountry={handleSearchCountry}
                    setRegion={handleRegion}
                    setSubRegion={handleSubregion}
                    setSortby={handleSortBy}
                    searchedSubRegion={SubRegion}
                    searchWithRegion={Region}
                    sortBy={sortBy}
                />
                {filteredCountries.length !== 0 ? (
                    <CountriesContainer countries={filteredCountries} />
                ) : (
                    <Error error="No such country found !" />
                )}
            </>
        )
    }
}

export default Home
