import React from "react"
import Filter from "./Filter"
import SearchInput from "./SearchInput"

const SearchBar = ({
    uniqueRegion,
    setSearchedCountry,
    setRegion,
    setSubRegion,
    setSortby,
    searchWithRegion,
    searchedSubRegion,
    sortBy,
}) => {
    return (
        <>
            <div className="inputs_container">
                <SearchInput setCountry={setSearchedCountry} />
                <Filter
                    options={Object.keys(uniqueRegion)}
                    selector={setRegion}
                    value={searchWithRegion}
                    text="Region"
                />
                <Filter
                    options={uniqueRegion[searchWithRegion]}
                    selector={setSubRegion}
                    value={searchedSubRegion}
                    text="SubRegion"
                />
                <Filter
                    options={[
                        "Area (low to high)",
                        "Area (high to low)",
                        "Population (low to high)",
                        "Population (high to low)",
                    ]}
                    text={"Sort by"}
                    selector={setSortby}
                    value={sortBy}
                />
            </div>
        </>
    )
}

export default SearchBar
