import React from "react"
import SearchIcon from "../assets/search.png"
import { useContext } from "react"
import { themeContext } from "../ThemeProvider"

const SearchInput = ({ setCountry }) => {
    const theme = useContext(themeContext)

    const background = theme["themeStyling"][theme["darkTheme"]]["elements"]

    return (
        <>
            <div className="search_bar">
                <label htmlFor="Search_country_content" >
                    <img src={SearchIcon} alt="search" />
                </label>
                <input
                    type="text"
                    name=""
                    id=""
                    className="search"
                    placeholder="Search for a country..."
                    onChange={(e) => setCountry(e.target.value)}

                    style={{ backgroundColor: `${background}`}}
                />
            </div>
        </>
    )
}

export default SearchInput
